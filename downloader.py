import urllib2
import sys

def download(url):
    file_name = url.split('/')[-1]
    u = urllib2.urlopen(url)
    f = open(file_name, 'wb')
    meta = u.info()
    file_size = int(meta.getheaders("Content-Length")[0])
    print "Downloading: %s Bytes: %s" % (file_name, file_size)
    print " "

    file_size_dl = 0
    counter = 0
    block_sz = file_size / 50;
    while True:
        buffer = u.read(block_sz)
        if not buffer:
            break
    
        file_size_dl += len(buffer)
        counter += 1;
    
        f.write(buffer)
        f.flush()
    
        status = "\r[%3.2f%%] <" % (file_size_dl * 100. / file_size)
        for x in range(0, counter):
            status += "-"
        for x in range(0, 50-counter):
            status += " "
        status += "> %s" %(file_size_dl)

        sys.stdout.flush()
        print status,
    
    f.close()

if __name__ == "__main__":
    url = "http://srv3.cinehub24.com/03/76934-55832-Andreea-Banica-Ft.-Dony-Samba.(1080p).mkv-[www.keepvid.com].mp4"
    download(url)
